package dk.groenbaek.stateless;

import lombok.Data;

/**
 * @author Klaus Groenbaek
 *
 */
@Data
public class ZipCode {

    String postalCode;
    String placeName;
    String stateName;
    String stateAbbreviation;
    String county;
    String latitude;
    String longitude;
}
