package dk.groenbaek.stateless;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import lombok.SneakyThrows;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Klaus Groenbaek
 *
 */
@Service
public class ZipCodeService {


    public List<ZipCode> getAll() {
        String fileName = "postal_codes_all.csv";
        return loadZipCodes(fileName);
    }

    public List<ZipCode> getSection(char startDigit) {
        if (!Character.isDigit(startDigit)) {
            throw new IllegalStateException("Must be a digit");
        }
        String fileName = "postal_codes_" + startDigit + ".csv";
        return loadZipCodes(fileName);
    }

    @SneakyThrows(IOException.class)
    private List<ZipCode> loadZipCodes(String fileName) {
        ClassPathResource resource = new ClassPathResource(fileName);
        try (InputStream inputStream = resource.getInputStream()) {
            CsvMapper mapper = new CsvMapper();
            CsvSchema schema = CsvSchema.emptySchema().withHeader().withColumnSeparator(',');
            MappingIterator<ZipCode> iterator = mapper.readerFor(ZipCode.class).with(schema).readValues(inputStream);
            ArrayList<ZipCode> result = new ArrayList<>();
            iterator.forEachRemaining(result::add);
            return result;
        }
    }

    public PageResult page(PageRequest request) {
        List<ZipCode> all = getAll();
        int start = Math.min(request.start(), all.size());
        int end = Math.min(request.end(), all.size());
        List<ZipCode> zipCodes = all.subList(start, end);

        return new PageResult()
                .setPageNumber(request.getPageNumber()).setPageSize(request.getPageSize())
                .setResults(zipCodes).setTotalResults(all.size());
    }
}
