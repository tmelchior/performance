package dk.groenbaek.stateless;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author Klaus Groenbaek
 *
 */
@Data
@Accessors(chain = true)
public class PageResult{
    private int pageNumber;
    private int pageSize;
    private int totalResults;
    private List<ZipCode> results;

}
