package dk.groenbaek.memoryusage.dto;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Klaus Groenbaek
 *
 */
@Data
public class Basket {

    private Map<Integer, LineItem> items = new HashMap<>();

    /**
     * Each basket gets an extra 256K so they take up more memory
     */
    private byte[] dummy = new byte[128000];

    public Basket addItem(Product product, int amount) {
        LineItem lineItem = items.get(product.getId());
        if (lineItem == null) {
            if (amount < 0) {
                throw new IllegalStateException("Quantiry can't be negative");
            }
            lineItem = new LineItem().setProduct(product);
            items.put(product.getId(), lineItem);
        }
        lineItem.incrementBy(amount);
        if (lineItem.getAmount() == 0) {
            items.remove(product.getId());
        }
        return this;
    }

    public double getTotal() {
        double sum = 0;
        for (LineItem lineItem : items.values()) {
            sum += lineItem.getAmount() * lineItem.getProduct().getPricePerItem();
        }
        return sum;
    }
}
