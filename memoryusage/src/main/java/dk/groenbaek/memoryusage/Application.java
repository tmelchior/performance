package dk.groenbaek.memoryusage;

import org.apache.catalina.Context;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatContextCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.context.annotation.Bean;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Klaus Groenbaek
 *
 */
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    /**
     * Configure Tomcat for session cleanup
     */
    @Bean
    public EmbeddedServletContainerCustomizer servletContainerCustomizer() {
        return new EmbeddedServletContainerCustomizer() {

            @Override
            public void customize(ConfigurableEmbeddedServletContainer container) {
                if (container instanceof TomcatEmbeddedServletContainerFactory) {
                    TomcatEmbeddedServletContainerFactory factory = (TomcatEmbeddedServletContainerFactory) container;
                    TomcatContextCustomizer contextCustomizer = new TomcatContextCustomizer() {
                        @Override
                        public void customize(Context context) {
                            context.setBackgroundProcessorDelay(10);
                        }
                    };
                    List<TomcatContextCustomizer> contextCustomizers = new ArrayList<>();
                    contextCustomizers.add(contextCustomizer);
                    factory.setTomcatContextCustomizers(contextCustomizers);
                }
            }
        };
    }


}
