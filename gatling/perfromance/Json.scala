package computerdatabase.perfromance

class Json extends Simulation {

  val httpConf = http
    .baseURL("http://localhost:8080")
    .acceptHeader("application/json")
    .contentTypeHeader("application/json")


  val scn = scenario("Stateless")
    .repeat(100, "counterName") {
      exec(http("Json").post("/zip/page").body(StringBody("""{"pageNumber": "${counterName}",  "pageSize" : 10}""")))
    }
  setUp(scn.inject(rampUsers(5) over (5 seconds)).protocols(httpConf))
}
