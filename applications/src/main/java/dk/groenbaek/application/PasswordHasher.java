package dk.groenbaek.application;

import com.google.common.io.BaseEncoding;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Hashes a password using a salt so it can be safely stored in a database
 *
 * @author Klaus Groenbaek
 *
 */
public final class PasswordHasher {

    private PasswordHasher() {
    }

    public static String hash(CharSequence password, long salt, int iterations) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            String saltedPassword = Long.toHexString(salt) + password.toString();
            byte[] bytes = saltedPassword.getBytes(StandardCharsets.UTF_8);
            for (int i=0; i< iterations; i++) {
                bytes = digest.digest(bytes);
            }
            return BaseEncoding.base32Hex().encode(bytes);
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException("Could not instantiate SHA-256", e);
        }
    }
}
