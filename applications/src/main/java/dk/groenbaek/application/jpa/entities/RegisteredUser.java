package dk.groenbaek.application.jpa.entities;

import dk.groenbaek.application.PasswordHasher;
import dk.groenbaek.application.jpa.JPAUtils;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.security.SecureRandom;
import java.util.Collection;

/**
 * @author Klaus Groenbaek
 *
 */
@Entity
@Getter
@Setter
@Accessors(chain = true)
public class RegisteredUser {   // user is often a registered keyword

    private static final SecureRandom SECURE_RANDOM = new SecureRandom();

    public static RegisteredUser create(String userName, CharSequence password) {
        long salt = SECURE_RANDOM.nextLong();
        String hash = PasswordHasher.hash(password, salt, 100 * 1000);
        return new RegisteredUser().setUsername(userName).setPasswordHash(hash).setSalt(salt);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.NONE)
    private Long id;
    @Column(unique = true)
    private String username;

    @Column
    @Setter(AccessLevel.PRIVATE)
    private String passwordHash;

    @Column
    @Setter(AccessLevel.PRIVATE)
    private long salt;

    @JoinColumn()
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private Collection<UserEvent> events;

    @JoinColumn()
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private Collection<Token> tokens;

    public boolean validPassword(CharSequence password) {
        return PasswordHasher.hash(password, salt, 100 * 1000).equals(passwordHash);
    }

    public Collection<Token> getTokens() {
        return tokens = JPAUtils.assignCollection(tokens);
    }

    public Collection<UserEvent> getEvents() {
        return events = JPAUtils.assignCollection(events);
    }

}
