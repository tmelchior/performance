package dk.groenbaek.application.jpa.repositories;

import dk.groenbaek.application.jpa.entities.RegisteredUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * @author Klaus Groenbaek
 *
 */
@Repository
public interface UserRepository extends JpaRepository<RegisteredUser, Long> {

    @Query("SELECT u FROM RegisteredUser u where u.username = :userName")
    RegisteredUser findUser(@Param("userName") String userName);
}
