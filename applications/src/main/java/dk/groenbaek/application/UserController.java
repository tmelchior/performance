package dk.groenbaek.application;

import dk.groenbaek.application.dto.ApiAuthentication;
import dk.groenbaek.application.dto.AuthenticateRequest;
import dk.groenbaek.application.dto.Status;
import dk.groenbaek.application.dto.UserInfo;
import dk.groenbaek.application.jpa.repositories.TokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.MimeTypeUtils;
import org.springframework.web.bind.annotation.*;

/**
 * @author Klaus Groenbaek
 *
 */
@Controller
@RequestMapping(value = "/user", produces = MimeTypeUtils.APPLICATION_JSON_VALUE, consumes = MimeTypeUtils.APPLICATION_JSON_VALUE)
public class UserController {

    /**
     * After login the user must send the returned token as the value of the X_AUTH_TOKEN header when invoking protected
     * endpoints
     */
    public static final String TOKEN_HEADER = "X-AUTH-TOKEN";

    @Autowired
    private UserService userService;

    @Autowired
    private TokenRepository tokenRepository;

    @RequestMapping(value = "/authenticate")
    @ResponseBody
    public ApiAuthentication authenticate(@RequestBody AuthenticateRequest request) {
        return userService.authenticate(request.getUserName(), request.getPassword());
    }

    /**
     * This method deliberately return 200 OK even if there is an error
     */
    @RequestMapping(value = "/signup")
    @ResponseBody
    public Status signup(@RequestBody AuthenticateRequest request) {
        return userService.signup(request.getUserName(), request.getPassword());
    }

    @RequestMapping(value = "/info", method = RequestMethod.GET)
    @ResponseBody
    public UserInfo userInfo(@RequestHeader(value = UserController.TOKEN_HEADER, required = false) String tokenValue) {
        return userService.userInfo(tokenValue);
    }



}
