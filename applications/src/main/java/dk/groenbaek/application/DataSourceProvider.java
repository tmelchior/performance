package dk.groenbaek.application;

import org.apache.derby.jdbc.EmbeddedDataSource;
import org.postgresql.ds.PGPoolingDataSource;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;

/**
 * @author Klaus Groenbaek
 *
 */
@Component
public class DataSourceProvider {

    public static final String DATABASE = "database";

    public DataSource getDataSource() {
        String database = System.getProperty(DATABASE);
        if (database == null) {
            EmbeddedDataSource ds = new EmbeddedDataSource();
            ds.setDatabaseName("test3");
            ds.setCreateDatabase("create");
            return ds;
        }
        else if ("postgresql".equals(database.toLowerCase())) {
            PGPoolingDataSource source = new PGPoolingDataSource();
            source.setDataSourceName("name");
            source.setServerName("localhost");
            source.setDatabaseName("performance");
            source.setUser("performance");
            source.setPassword("performance");
            source.setMaxConnections(2);
            return source;
        }
        throw new IllegalStateException(database + " is not supported");
    }

    public Database getDatabase() {
        String database = System.getProperty(DATABASE);
        if (database == null) {
            return Database.DERBY;
        }
        else if ("postgresql".equals(database.toLowerCase())) {
            return Database.POSTGRESQL;
        }
        throw new IllegalStateException(database + " is not supported");

    }





}
